const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
// const devMode = process.env.NODE_ENV !== 'developent';
const HtmlWebpackPlugin = require("html-webpack-plugin");

module.exports = {
  entry: './src/index.js',
  mode: 'development',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundle.[hash].js'
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: "stylesheets/[name].[contenthash:8].css",
      chunkFilename: "[id].css",
      ignoreOrder: false,
    }),
    new HtmlWebpackPlugin({
      template: "./src/template/plug.html",
      filename: 'plug.html',
      minify: {
        html5: true,
        preserveLineBreaks: true,
        decodeEntities: true
      }
    })
  ],
  module:{
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules)/,
        loader: 'babel-loader',
        options: {
          presets: ['@babel/env']
        }
      },
      {
        test: /\.(sa|sc|c)ss$/,
        resolve: {extensions: [".scss", ".css"],},
        use    : [
          'style-loader',
          MiniCssExtractPlugin.loader,
          'css-loader',
          // 'postcss-loader?sourceMap',
          // 'resolve-url-loader?sourceMap',
          'sass-loader?sourceMap',
        ]
        },
      {
        test: /\.html$/,
        use: ["html-loader"]
      },
      {
        test: /\.(svg|png|jpe?g|gif)$/,
        use: {
          loader: "file-loader",
          options: {
            name: "[name].[hash].[ext]",
            outputPath: "images"
          }
        }
      }
    ]
  },
  devServer: {
    contentBase: path.join(__dirname, 'dist'),
    compress: true,
    port: 9000
  },
};